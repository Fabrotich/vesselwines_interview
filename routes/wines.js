// Required packages
var mongoose = require('mongoose'),
    express = require('express'),
    router = express.Router(),
    fs = require('fs'),
    path = require('path'),
    os = require('os'),
    Busboy = require('busboy');

// Import database schemas
var Wine = require('../models/wine');

router.route('/catalogue')
    // Query for all posts
    .get(function (req, res) {
        Wine.find({})
            .exec(function (err, wines) {
                if (err)
                    res.send(err);

                if (!wines.length) {
                    res.json({ status: 0, message: "Catalogue Empty" });
                } else {
                    res.json({ status: 1, message: wines });
                }

            })
    });

router.route('/:wine_id')
    // Query for a specific post
    .get(function (req, res) {
        Wine.find({ _id: req.params.wine_id })
            .exec(function (err, wine) {
                if (err)
                    res.send(err);

                if (!wine.length) {
                    res.json({ status: 0, message: "Wine not found." });
                } else {
                    res.json({ status: 1, message: wine });
                }
            })
    })

    // Update wine info
    .put(function (req, res) {

        Wine.findById(req.params.wine_id, function (err, wine) {
            if (err)
                res.send(err);
            if (wine !== null) {
                // Update wine information
                wine.name = req.body.name;
                wine.year = req.body.year;
                wine.type = req.body.type;
                wine.bottlePrice = req.body.bPrice;
                wine.cratePrice = req.body.cPrice;
                wine.rating = req.body.rating;

                // Save the wine changes
                wine.save(function (err) {
                    if (err)
                        res.send(err);

                    res.json({ status: 1, message: 'Wine updated.' })
                });
            } else {
                res.json({ status: 0, message: 'Wine to update not found.' });
            }

        });
    })

    // Delete wine
    .delete(function (req, res) {
        Wine.remove({ _id: req.params.wine_id }, function (err, wine) {
            if (err)
                res.send(err);

            res.json({ status: 1, message: 'Wine deleted.' });
        })
    });

router.route('/add_wine')
    // Add wine to catalogue
    .post(function (req, res) {
        /** 
           --Random numbers are appended to the new path for uniqueness.
       */
        var fileExtension, fileName, storagePath;
        var busboy = new Busboy({ headers: req.headers });
        busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
            // Generate random filename
            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }

                return s4() + s4() + s4();
            }

            fileName = guid() + path.basename(filename);
            storagePath = process.cwd() + '/public/bottle_images/' + fileName;

            file.pipe(fs.createWriteStream(storagePath));
        });

        // On getting a non-file field
        var name, year, type, bottlePrice, cratePrice, rating;
        busboy.on('field', function (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
            switch (fieldname) {
                case "name":
                    name = val
                    break;
                case "year":
                    year = val
                    break;
                case "type":
                    type = val
                    break;
                case "bPrice":
                    bottlePrice = val
                    break;
                case "cPrice":
                    cratePrice = val
                    break;
                case "rating":
                    rating = val
                    break;
            }
        });

        busboy.on('finish', function () {
            var wine = new Wine({
                name: name,
                year: year,
                type: type,
                bottlePrice: bottlePrice,
                cratePrice: cratePrice,
                rating: rating,
                imgPath: storagePath
            });

            wine.save(function (err) {
                if (err)
                    res.send(err);

                res.json({ status: 1, message: 'Wine added successfully.' });
            })
        });

        req.pipe(busboy);
    });

module.exports = router;
