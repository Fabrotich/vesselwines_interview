vesselwines.controller('shopCtrl', function ($scope, $state, $http) {
    var baseUrl = "http://localhost:8080/api";
    $scope.totalPrice = 0;
    $scope.cartItems = [];

    // Get Wine Catalogue
    $http.get(baseUrl + '/wines/catalogue')
        .success(function (data) {
            if (data.status === 1) {
                var wines = data.message;
                for (var wine in wines) {
                    var tempPath = wines[wine].imgPath;
                    wines[wine].imgPath = tempPath.replace('/opt/lampp/htdocs/VesselWines _Backend/public/', 'http://localhost:8080/');
                }
                $scope.wines = wines;
            }
        })
        .error(function (data) {
            console.log(data);
        });

    $scope.addToCart = function (wine, bottleNum, cratesNum) {
        var name = wine.name,
            year = wine.year,
            bottles = 0,
            cratesTmp = 0,
            crates = 0;

        console.log('bottles1', bottles)
        if (bottleNum) {
            bottles = bottleNum;
            console.log('bottles2', bottles)
        }
        if (cratesNum) {
            crates = (cratesNum * 12);
            cratesTmp = cratesNum
        }

        var totBottles = crates + bottles,
            price = (bottles * wine.bottlePrice) + (cratesTmp * wine.cratePrice);

        console.log('totBottles', totBottles)
        $scope.totalPrice += price;
        $scope.cartItems.push({ name: name, year: year, bottles: totBottles });
    }

    $scope.removeItem = function (index) { 
        $scope.cartItems.splice(index, 1);
     }
});