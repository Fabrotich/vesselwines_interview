var vesselwines = angular.module('vesselwines', ['ui.router']);

vesselwines.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/shop');

    $stateProvider
        .state('shop', {
            url: '/shop',
            templateUrl: 'partials/shop.html',
            controller: 'shopCtrl'
        });


});
