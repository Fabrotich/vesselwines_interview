var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var wineSchema = new Schema({
    name: { type: String },
    year: { type: Number },
    type: {type: String},
    bottlePrice: { type: Number },
    cratePrice: { type: Number },
    rating: {type: Number},
    imgPath: {type: String}
});

module.exports = mongoose.model('wine', wineSchema);