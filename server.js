// Needed packages
var express = require('express');
var app = express();
var router = express.Router();
var bodyParser = require('body-parser');
var mongoose = require("mongoose");

// Routes
var wines = require('./routes/wines');

mongoose.connect("mongodb://localhost/vesselwines");

// Configuring the app to use body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Route handlers
app.use(function (req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', '*');

  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  res.setHeader('Access-Control-Allow-Credentials', true);

  next();

});

// Route handling
router.use('/wines', wines);

// Setting the port for the server to listen to
var port = process.env.PORT || 8080;

// Registering the routes
// All routes will be prefixed with /api
app.use('/api', router);

// Static files
app.use('/', express.static('public'));

// Start the server
app.listen(port);
console.log("Server started at " + port);